<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

require_once 'class/mandrill.class.php';

if (isset($_POST['data'])) {
    $data=$_POST['data'];
    $response = array('pl' => '', 'er' => '');
    $key = 'EC4d53m1lN76pcmOGjLC2A';
    $to_email = $data['to_email'];
    $to_name = 'John Smith';
    $message = $data['message'];
    $subject = ($data['subject']!='')?$data['subject']:'Your next appointment';
    
    $email = new mandrill($key);
    
    $email->to($to_email,$to_name);
    $email->from('celsoluiz81@gmail.com','Celso Fernandes');
    $email->message($message);
    $email->subject($subject);
    $result=$email->sendEmail();
    if ($result) {
        $response['pl'] = $result;
    } else {      
        $response['er'] = $email->getError();
    }
    print json_encode($response);
}
?>
