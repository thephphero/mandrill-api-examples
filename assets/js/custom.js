/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

jQuery(document).ready(function ($) {

    var form = $('#form');

    form.close = function () {
        $(this).slideUp("slow", function () {
            form.clear();
        });
    };

    form.toggle = function () {
        this.slideToggle("slow", function () {
            return false;
        });
    };

    form.validate = function () {
        var email = $('#to_email').val();
        var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
        if (filter.test(email)) {
            return true;
        }
        else {
            return false;
        }
    };

    form.send = function () {
        var ajaxForm = $('form.ajaxForm');
        var url = ajaxForm.attr('action');
        if (this.validate()) {
            $('#to_email').css('background-color','#FFF');
            $.ajax({
                type: "POST",
                url: url,
                data: ajaxForm.serialize(),
                success: function (data)
                {
                    $('.avatar').attr('src', 'assets/img/success.png');
                    form.close();
                },
                error: function (error) {
                    $('.avatar').attr('src', 'assets/img/error.png');
                }
            });
            return;
        }
        else{
            $('#to_email').css('background-color','#E74C3C');
            return false;
        }
    };

    form.clear = function () {
         $('#to_email').css('background-color','#FFF');
        form.find("input[type=text],input[type=email] ,textarea").val("");
        return;
    };


    $(".sendmail").click(function (event) {
        event.preventDefault();
        form.toggle();
    });

    $('.cancel').on('click', function (event) {
        event.preventDefault();
        form.close();
    });

    $('.send').on('click', function (event) {
        event.preventDefault();
        form.send();
    });

});
