<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class mandrill {

    private $apiKey = '';
    private $uri = 'https://mandrillapp.com/api/1.0/messages/send.json';
    private $params = array('message' => array());
    private $to = array();
    private $images = array();
    private $attachments = array();
    private $message = '';
    private $from_name;
    private $from_email;
    private $subject;
    private $error;

    public function __construct($apiKey) {
        $this->apiKey = trim($apiKey);
    }

    /**
     * Email Validation
     *
     * @access	private
     * @param	string
     * @return	bool
     */
    private function validateEmail($address) {
        return (!preg_match("/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix", $address)) ? FALSE : TRUE;
    }

    /**
     * Set buildJson
     *
     * @access	private  
     * @return	string
     */
    private function buildJson() {
        $this->params['key'] = $this->apiKey;
        $this->params['message']['from_name'] = $this->from_name;
        $this->params['message']['from_email'] = $this->from_email;
        $this->params['message']['subject'] = $this->subject;
        $this->params['message']['html'] = $this->message;
        $this->params['message']['headers'] = array('Reply-To' => $this->from_email);
        foreach ($this->to as $rcp) {          
            $this->params['message']['to'][] = $rcp;
        }
       
        foreach ($this->attachments as $file) {
            $this->params['message']['attachments'][] = $file;
        }
        foreach ($this->images as $image) {
            $this->params['message']['images'][] = $image;
        }

        return json_encode($this->params);
    }

    /**
     * Set addParams
     *
     * @access	public
     * @param	string
     * @param	string
     * @return	int
     */
    public function addParams($key, $value = '') {
        return $this->params['message'][$key] = $value;
    }

    /**
     * Set from
     *
     * @access	public
     * @param	string
     * @param	string
     * @return	int
     */
    public function from($email, $name = '') {
        if (!$this->validateEmail($email)) {
            return false;
        }
        $this->from_name = trim($name);
        $this->from_email = trim($email);
        return $this;
    }

    /**
     * Set to
     *
     * @access	public
     * @param	string
     * @param	string
     * @return	int
     */
    public function to($email, $name = '') {
        if (!$this->validateEmail($email)) {
            return false;
        }
        $recipient = array(
            'name' => trim($name),
            'email' => $email,
            'type' => 'to'
        );
        return array_push($this->to, $recipient);
    }

    /**
     * Set cc
     *
     * @access	public
     * @param	string
     * @param	string
     * @return	int
     */
    public function cc($email, $name = '') {
        if (!$this->validateEmail($email)) {
            return false;
        }
        $recipient = array(
            'name' => trim($name),
            'email' => $email,
            'type' => 'cc'
        );
        return array_push($this->to, $recipient);
    }

    /**
     * Set bcc
     *
     * @access	public
     * @param	string
     * @param	string
     * @return	int
     */
    public function bcc($email, $name = '') {
        if (!$this->validateEmail($email)) {
            return false;
        }
        $recipient = array(
            'name' => trim($name),
            'email' => $email,
            'type' => 'bcc'
        );
        return array_push($this->to, $recipient);
    }

    /**
     * Set addAtachment
     *
     * @access	public
     * @param	string
     * @param	string
     * @param	string
     * @return	int
     */
    public function addAttachment($type, $name, $path) {
        $file = file_get_contents($path);
        $content = base64_encode($file);
        if (!$content) {
            return false;
        }
        $attachment = array(
            'name' => trim($name),
            'contents' => trim($content),
            'type' => $type
        );
        return array_push($this->attachments, $attachment);
    }

    /**
     * Set addImages
     *
     * @access	public
     * @param	string
     * @param	string
     * @param	string
     * @return	int
     */
    public function addImage($type, $name, $path) {
        $file = file_get_contents($path);
        $content = base64_encode($file);
        if (!$content) {
            return false;
        }
        $image = array(
            'name' => trim($name),
            'contents' => trim($content),
            'type' => $type
        );
        return array_push($this->images, $image);
    }

    /**
     * Set subject
     *
     * @access	public
     * @param	string
     * @return	void
     */
    public function subject($subject) {
        $this->subject = stripslashes($subject);
        return $this;
    }

    /**
     * Set message
     *
     * @access	public
     * @param	string
     * @return	void
     */
    public function message($message) {
        $body = rtrim(str_replace("\r", "", $message));
        $this->message = stripslashes($body);
        return $this;
    }

    /**
     * Set clear
     *
     * @access	public
     * @param	bool
     * @return	void
     */
    public function clear($clear_attach = FALSE) {
        $this->from_email = '';
        $this->from_name = '';
        $this->subject = '';
        $this->message = '';
        $this->params = array();
        $this->to = array();
        if ($clear_attach) {
            $this->images = array();
            $this->attachments = array();
        }
        return $this;
    }

    /**
     * Set sendEmail
     *
     * @access	public
     * @return	bool
     */
    public function sendEmail() {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->uri);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $this->buildJson());
        $response = curl_exec($ch);
        if (!$response) {
            $this->error = curl_error($ch);
            return false;
        }
        curl_close($ch);
        return $response;
    }

    /**
     * Set getError
     *
     * @access	public
     * @return	string
     */
    public function getError() {
        return $this->error;
    }

}
