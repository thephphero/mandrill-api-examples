<?php

//change these values with your own!
$from_name = 'Celso Fernandes';
$from_email = 'celso@thephphero.com';
$to_name = 'John Jensen';
$to_email = 'john@smartshanghai.com';
$subject = 'PHP Script';
$message = 'My PHP email function is working.';
$my_key = 'EC4d53m1lN76pcmOGjLC2A';
sendEmail($my_key,$from_name, $from_email, $to_name, $to_email, $subject, $message);

/**
 * Set sendEmail
 *
 * @param	string
 * @param	string
 * @param	string
 * @param	string
 * @param	string
 * @param	string
 * @return	string
 */
function sendEmail($my_key,$from_name, $from_email, $to_name, $to_email, $subject, $message) {
    $uri = 'https://mandrillapp.com/api/1.0/messages/send.json';
   
    $data = array();
    $data['key'] = $my_key;
    $data['message']['html'] = '<p>' . $message . '</p>';
    $data['message']['text'] = '<p>' . $message . '</p>';
    $data['message']['subject'] = $subject;
    $data['message']['from_email'] = $from_email;
    $data['message']['from_name'] = $from_name;
    $data['message']['to'][] = array('name' => $to_name, 'email' => $to_email);
    $json = json_encode($data);

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $uri);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
    $response = curl_exec($ch);
    if ($response) {
        print($response);
        curl_close($ch);
        return true;
    } else {
        print(curl_error($ch));
        curl_close($ch);
        return false;
    }
}

?>