<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

require_once 'class/mandrill.class.php';
//Please change with your values!
$key='EC4d53m1lN76pcmOGjLC2A';
$to_email='celso@thephphero.com';
$to_name='Celso Fernandes';
$image_path=dirname(__FILE__).'/assets/img/128.png';
$article_path=dirname(__FILE__).'/assets/attachment/article.pdf';

$email = new mandrill($key);

$email->to($to_email, $to_name);
$email->from('celsoluiz81@gmail.com', 'Celso Fernandes');
$email->message('This script sends attachments and images.');
$email->subject('PHP Script');
$email->addImage('image/png','128.png',$image_path);
$email->addAttachment('text/pdf','article.pdf',$article_path );
$response=$email->sendEmail();
if($response){
    print($response);
}
else{
    print($email->getError());
}