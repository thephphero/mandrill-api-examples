<?php

/*
 * --------------------------------------------------------------------
 * COMPOSER AUTOLOAD
 * --------------------------------------------------------------------
 */
include_once 'vendor/autoload.php';
try {
    $mandrill = new Mandrill('EC4d53m1lN76pcmOGjLC2A');
    
    $to_email = 'celsoluiz81@gmail.com';
    $to_name = 'Celso Fernandes';
    
    $from_name = 'Celso Fernandes';
    $from_email = 'no-reply@thephphero.com';
   
    $subject = 'PHP Script';
    $message = 'This script makes use of composer. Have a good day!';
    $my_key = 'EC4d53m1lN76pcmOGjLC2A';
    
    $content =array(
        'key'=>$my_key,
        'html'=>'<p>'.$message.'</p>',
        'text'=>$message,
        'subject'=>$subject,
        'from_email'=>$from_email,
        'from_name'=>$from_name,
        'to'=>array(
            array('email'=>$to_email,'name'=>$to_name)
        ), 
        'headers'    => array(
            'Reply-To' => $from_email
        )
    ); 
    
    $result = $mandrill->messages->send($content);
    print_r($result);
} catch (Mandrill_Error $e) {
    echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
    throw $e;
}
?>
